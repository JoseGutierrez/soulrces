import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
//import 'package:hive/hive.dart';
import 'package:soulrces/src/models/story.dart';
import 'src/MyApp.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();

  Hive.registerAdapter(StoryAdapter());
  await Hive.openBox<Story>('stories');

  runApp(MyApp());
}

const buttonColor = Color(0xff88CBFF);
