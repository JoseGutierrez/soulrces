import 'package:flutter/material.dart';
//import 'package:hive/hive.dart';
import 'package:soulrces/src/pages/home_page.dart';
import 'package:soulrces/src/pages/login_page.dart';

// en esta clase se redirige al login
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        routes: {'home': (BuildContext context) => HomePage()},
        home: LoginAndRegister());
  }
}
