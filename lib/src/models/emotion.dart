import 'package:flutter/cupertino.dart';

class Emotion {
  final String emotion;
  final String emote;
  final Color color;

  const Emotion(
      {required this.emotion, required this.color, required this.emote});
}

const List<Emotion> emotionList = [
  Emotion(
      emotion: 'Feliz',
      emote: '😄',
      color: Color(0xffFFFB7D)), // emocion 1 feliz
  Emotion(
      emotion: 'Confiado',
      emote: '😁',
      color: Color(0xff88F58E)), // emocion2  Confiado
  Emotion(
      emotion: 'Asustado',
      emote: '😰',
      color: Color(0xff87F5BF)), //emocion 3 Asustado
  Emotion(
      emotion: 'Sorprendido',
      emote: '😮',
      color: Color(0xff88CBFF)), //emocion 4 Sorprendido
  Emotion(
      emotion: 'Triste',
      emote: '😥',
      color: Color(0xff7F7EF5)), //emocion 5 Triste
  Emotion(
      emotion: 'Enfermo',
      emote: '🤢',
      color: Color(0xffFFA2FB)), //emocion 6 Aversion significado de enfermo
  Emotion(
      emotion: 'Furioso',
      emote: '😡',
      color: Color(0xffFF8F9A)), //emocion 7 Iracundo - furioso
  Emotion(
      emotion: 'Angustiado',
      emote: '😩',
      color: Color(0xffFFCF61)) //emocion 8 Angustiado
];
