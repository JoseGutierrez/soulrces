import 'package:hive/hive.dart';

part 'story.g.dart';

@HiveType(typeId: 0)
class Story extends HiveObject{
  @HiveField(0)
  late String story;

  @HiveField(1)
  late String emotion;

  @HiveField(2)
  late int colorEmotion;

  @HiveField(3)
  late DateTime createdDate;
}
