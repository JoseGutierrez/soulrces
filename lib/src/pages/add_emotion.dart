import 'package:flutter/material.dart';
import 'package:soulrces/src/pages/write_emotion.dart';

import '../../main.dart';
import 'home_page.dart';

class AddEmotionPage extends StatelessWidget {
  final int emotionSelected;

  const AddEmotionPage({Key? key, required this.emotionSelected})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: Container(
        height: size.height * .72,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomAppBar(
              iconData: Icons.arrow_back_ios_rounded,
              callback: () => Navigator.pop(context),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Selecciona de qué \nmanera contarnos tu relato',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            _Options(
              size: size,
              emotionSelected: emotionSelected,
            ),
          ],
        ),
      )),
    );
  }
}

class _Options extends StatefulWidget {
  final Size size;
  final int emotionSelected;

  const _Options({Key? key, required this.size, required this.emotionSelected})
      : super(key: key);

  @override
  __OptionsState createState() => __OptionsState();
}

class __OptionsState extends State<_Options> {
  final listIcons = [Icons.mic_rounded, Icons.insert_drive_file];

  bool isSelectedItem = false;
  int selectedItem = -1;

  @override
  Widget build(BuildContext context) {
    final size = widget.size.height * .1;
    return Container(
      height: widget.size.height * .4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(
              listIcons.length,
              (i) => GestureDetector(
                onTap: () {
                  setState(() {
                    isSelectedItem = true;
                    selectedItem = i;
                  });
                },
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  height:
                      isSelectedItem && selectedItem == i ? size * 1.2 : size,
                  width:
                      isSelectedItem && selectedItem == i ? size * 1.2 : size,
                  decoration: BoxDecoration(
                      color: Color(0xffFFCF61),
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0, 4),
                            blurRadius: 4)
                      ]),
                  child: Icon(
                    listIcons[i],
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: buttonColor,
                fixedSize: Size(widget.size.width * .8, 40)),
            child: Text(
              'Agregar emoción',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: isSelectedItem
                ? (selectedItem == 0)
                    ? () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WriteEmotionPage(
                                  emotionSelected: widget.emotionSelected,
                                )))
                    : () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WriteEmotionPage(
                                  emotionSelected: widget.emotionSelected,
                                )))
                : null,
          )
        ],
      ),
    );
  }
}
