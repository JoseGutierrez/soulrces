import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:soulrces/src/models/emotion.dart';

import '../../main.dart';
import 'add_emotion.dart';

class Emotions extends StatelessWidget {
  const Emotions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    User? user = FirebaseAuth.instance.currentUser;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${user!.displayName}, \nCómo te sientes hoy?',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: size.height * .08,
        ),
        _Emotions(size: size)
      ],
    );
  }
}

class _Emotions extends StatefulWidget {
  const _Emotions({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  __EmotionsState createState() => __EmotionsState();
}

class __EmotionsState extends State<_Emotions> {
  String? emotionSelected;
  late int emotion;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Me siento...',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
          ),
        ),
        SizedBox(
          height: 14,
        ),
        Container(
          height: 110,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(
                emotionList.length,
                (i) => GestureDetector(
                      onTap: () {
                        setState(() {
                          emotionSelected = emotionList[i].emotion;
                          emotion = i;
                        });
                      },
                      child: Container(
                        // margin: const EdgeInsets.only(left: 10, right: 4),
                        child: Column(
                          children: [
                            AnimatedContainer(
                              duration: Duration(milliseconds: 350),
                              height: emotionSelected == emotionList[i].emotion
                                  ? 70
                                  : 50,
                              width: widget.size.width * .09,
                              decoration: BoxDecoration(
                                  color: emotionList[i].color,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              emotionList[i].emote,
                              style: TextStyle(fontSize: 22),
                            )
                          ],
                        ),
                      ),
                    )),
          ),
        ),
        SizedBox(
          height: widget.size.height * .06,
        ),
        emotionSelected != null
            ? Text(
                '...$emotionSelected.',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              )
            : Text(''),
        SizedBox(
          height: widget.size.height * .06,
        ),
        emotionSelected != null
            ? ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: buttonColor,
                    fixedSize: Size(widget.size.width * .8, 40)),
                child: Text(
                  'Agregar emoción',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddEmotionPage(
                              emotionSelected: emotion,
                            ))),
              )
            : Container(
                height: 52,
              )
      ],
    );
  }
}
