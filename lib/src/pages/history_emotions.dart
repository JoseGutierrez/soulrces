import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:soulrces/src/models/story.dart';
import 'package:soulrces/src/services/story_service.dart';

class HistoryEmotions extends StatelessWidget {
  const HistoryEmotions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<Story>>(
      valueListenable: Boxes.getTransactions().listenable(),
      builder: (context, box, _) {
        final stories = box.values.toList().cast<Story>();

        if (stories.isEmpty) {
          return Center(
            child: Text(
              'No hay historias para ver',
              style: TextStyle(fontSize: 24),
            ),
          );
        } else {
          return Expanded(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: stories.length,
              itemBuilder: (context, i) {
                final story = stories[i];
                return Container(
                  height: 40,
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  child: Row(
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                            color: Color(story.colorEmotion),
                            borderRadius: BorderRadius.circular(6)),
                        child: Icon(Icons.insert_drive_file),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Text(story.story),
                      Spacer(),
                      GestureDetector(
                          onTap: () {
                            final storyService = StoryService();
                            storyService.deleteStory(story);
                          },
                          child: Icon(Icons.delete))
                    ],
                  ),
                );
              },
            ),
          );
        }
      },
    );
  }
}
