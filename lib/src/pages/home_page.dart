import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'emotions.dart';
import 'history_emotions.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedItem = 0;
  List<Widget> views = [Emotions(), HistoryEmotions()];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size.height,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
          child: Column(
            children: [
              CustomAppBar(
                iconData: Icons.menu,
                callback: () {},
              ),
              SizedBox(
                height: size.height * .06,
              ),
              views[_selectedItem],
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedItem,
        elevation: 0,
        onTap: (i) {
          setState(() {
            _selectedItem = i;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 26,
              ),
              label: ''),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.list,
                size: 26,
              ),
              label: '')
        ],
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget {
  final IconData iconData;
  final VoidCallback callback;

  const CustomAppBar({Key? key, required this.iconData, required this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(onTap: callback, child: Icon(iconData)),
        Container(
          width: 80,
          height: 80,
          child: Image(
            image: AssetImage('assets/logo.png'),
            fit: BoxFit.cover,
          ),
        ),
        // CircleAvatar(
        //   radius: 40,
        //   backgroundImage: AssetImage('assets/logo.png'),
        //   backgroundColor: Colors.transparent,
        // ),
        ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Container(
            width: 40,
            height: 40,
            child: Image(
              image: NetworkImage(user!.photoURL!),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }
}
