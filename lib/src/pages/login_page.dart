import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:soulrces/src/services/google_signin.dart';

import '../../main.dart';
import 'home_page.dart';

PageController _pageController = new PageController();

class LoginAndRegister extends StatefulWidget {
  LoginAndRegister({Key? key}) : super(key: key);

  @override
  _LoginAndRegisterState createState() => _LoginAndRegisterState();
}

class _LoginAndRegisterState extends State<LoginAndRegister> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: [LoginPage(), RegisterPage()],
      ),
    );
  }
}

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(18),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: size.height * .1,
            ),
            Image.asset(
              'assets/logo.png',
              height: size.height * .14,
            ),
            SizedBox(
              height: size.height * .05,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Inicia sesión',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
              ),
            ),
            SizedBox(
              height: size.height * .05,
            ),
            _Fiels(
              hintText: "Correo electrónico",
            ),
            SizedBox(
              height: 16,
            ),
            _Fiels(
              hintText: "Contraseña",
              isPass: true,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color(0xff88CBFF),
                    elevation: 0,
                    fixedSize: Size(size.width, 42)),
                child: Text(
                  'Iniciar sesión',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                onPressed: () => Navigator.pushNamedAndRemoveUntil(
                    context, 'home', (route) => false)),
            SizedBox(
              height: size.height * .06,
            ),
            GestureDetector(
              onTap: () async {
                final loginService = LoginService();
                await loginService.loginGoogle();
                Navigator.pushNamed(context, 'home');
              },
              child: Container(
                padding: EdgeInsets.all(3),
                margin: EdgeInsets.symmetric(horizontal: size.width * 0.2),
                width: double.infinity,
                height: 45,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(7),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(2, 1),
                          blurRadius: 6,
                          color: Color.fromARGB(255, 232, 232, 232))
                    ]),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FaIcon(
                        FontAwesomeIcons.google,
                        color: Color(0xff515151),
                        size: 22,
                      ),
                      Text(
                        "Ingrese con Google",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ]),
              ),
            ),
            SizedBox(
              height: size.height * .1,
            ),
            GestureDetector(
              onTap: () {
                _pageController.animateToPage(_pageController.page!.toInt() + 1,
                    duration: Duration(milliseconds: 800),
                    curve: Curves.fastOutSlowIn);
              },
              child: RichText(
                text: TextSpan(
                    text: '¿No tienes cuenta?',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                    ),
                    children: [
                      TextSpan(
                          text: ' Regístrate',
                          style: TextStyle(
                              color: buttonColor, fontWeight: FontWeight.bold))
                    ]),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(18),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: size.height * .1,
            ),
            Image.asset(
              'assets/logo.png',
              height: size.height * .14,
            ),
            SizedBox(
              height: size.height * .02,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Regístrate',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
              ),
            ),
            CircleAvatar(
              radius: 40,
              backgroundColor: Colors.grey[200],
            ),
            SizedBox(
              height: 24,
            ),
            _Fiels(
              hintText: "Nombre Completo",
            ),
            SizedBox(
              height: 16,
            ),
            _Fiels(
              hintText: "Correo electrónico",
              isPass: true,
            ),
            SizedBox(
              height: 16,
            ),
            _Fiels(
              hintText: "Contraseña",
              isPass: true,
            ),
            SizedBox(
              height: 26,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color(0xff88CBFF),
                    elevation: 0,
                    fixedSize: Size(size.width, 42)),
                child: Text(
                  'Regístrate',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false)),
            SizedBox(
              height: size.height * .1,
            ),
            GestureDetector(
              onTap: () {
                _pageController.animateToPage(_pageController.page!.toInt() - 1,
                    duration: Duration(milliseconds: 800),
                    curve: Curves.fastOutSlowIn);
              },
              child: RichText(
                text: TextSpan(
                    text: '¿Ya tienes cuenta?',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                    ),
                    children: [
                      TextSpan(
                          text: ' Inicia sesión',
                          style: TextStyle(
                            color: buttonColor,
                          ))
                    ]),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}

class _Fiels extends StatelessWidget {
  final String hintText;
  final bool isPass;

  const _Fiels({Key? key, required this.hintText, this.isPass = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      child: TextField(
        obscureText: isPass,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide.none,
          ),
          hintText: hintText,
          hintStyle: TextStyle(
              color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w500),
          contentPadding: EdgeInsets.only(left: 10),
          fillColor: Colors.grey[200],
          filled: true,
        ),
      ),
    );
  }
}
