import 'package:flutter/material.dart';
import 'package:soulrces/src/models/emotion.dart';
import 'package:soulrces/src/services/story_service.dart';

class WriteEmotionPage extends StatefulWidget {
  final int emotionSelected;

  const WriteEmotionPage({Key? key, required this.emotionSelected})
      : super(key: key);

  @override
  _WriteEmotionPageState createState() => _WriteEmotionPageState();
}

class _WriteEmotionPageState extends State<WriteEmotionPage> {
  final formKey = GlobalKey<FormState>();
  final _storyController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _storyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context)
        .size; //entra a las propiedades del tamaño del dispositivo

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.black,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Anécdota',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Form(
                  key: formKey,
                  child: TextFormField(
                    maxLines: 10,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        borderSide: BorderSide.none,
                      ),
                      hintText: "Describe la situación...",
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                    controller: _storyController,
                    validator: (story) =>
                        story != null && story.isEmpty && story.length <= 6
                            ? 'Por favor describe tu situación'
                            : null,
                  ),
                ),
                SizedBox(
                  height: size.height * .06,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color(0xff88CBFF),
                        elevation: 0,
                        fixedSize: Size(size.width, 42)),
                    child: Text(
                      'Agregar emoción',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      final isValid = formKey.currentState!.validate();
                      if (isValid) {
                        final storyService =
                            StoryService(); //se usan los metodos de story donde se escriben y eliminan datos
                        final story = _storyController.text;

                        storyService.addStory(
                            story,
                            emotionList[widget.emotionSelected].emotion,
                            emotionList[widget.emotionSelected].color.value);
                        Navigator.popUntil(
                            context, ModalRoute.withName('home'));
                      }
                    })
              ],
            ),
          ),
        ));
  }
}
