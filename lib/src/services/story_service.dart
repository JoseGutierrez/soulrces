import 'package:hive_flutter/hive_flutter.dart';
import 'package:soulrces/src/models/story.dart';

class StoryService {
  Future addStory(String story, String emotion, int color) async {
    //se asigna los datos a tomar
    final storydb =
        Story() //crea un objeto de tipo Story basado en el adaptador de hive
          ..story = story
          ..emotion = emotion
          ..colorEmotion = color
          ..createdDate = DateTime.now();

    final box = Boxes.getTransactions();
    box.add(storydb); //referencia  a la propiedad
  }

  void deleteStory(Story transaction) {
    transaction.delete(); //elimina el dato creado
  }
}

class Boxes {
  static Box<Story> getTransactions() => Hive.box<Story>(
      'stories'); //se obtienen la referencia de stories de la base de datos para poder leer y escribir
}
